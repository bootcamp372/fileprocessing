﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace TC
{
    internal class Timecard
    {
        public string Name { get; set; }
        public decimal HoursWorked { get; set; }
        public decimal PayRate { get; set; }

        public Timecard(string name, decimal hoursWorked, decimal payRate) 
        {
            this.Name = name;
            this.HoursWorked = hoursWorked;
            this.PayRate = payRate;
        }

        public static Timecard CreateTimeCard(string data)
        {

/*            data = "Gothie Capps|45|38.60"; // actually read // from file*/
            Timecard tc = Timecard.CreateEmployee(data);
            return tc;
        }

        public static Timecard CreateEmployee(string data) {
            if (data == null) { return null; };
            string[] dataItems = data.Split('|');
            Timecard employee = new Timecard(dataItems[0], Convert.ToDecimal(dataItems[1]), Convert.ToDecimal(dataItems[2]));
            return employee;
        }

        public decimal GetGrossPay()
        {
            decimal pay = 0;
            decimal overtime;
            decimal hours = HoursWorked;
            decimal payRate = PayRate;
            if (hours > 40) // if employee worked more than 40hrs
            {
                overtime = hours - 40;
                pay = hours * payRate;
                overtime = hours * (payRate * 1.5M);
                pay = pay + overtime;
            }
            else
            {
                pay = hours * payRate;
            }
            return pay;
        }

    }

}
