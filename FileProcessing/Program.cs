﻿using TC;
using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.ConstrainedExecution;
using System.Xml.Linq;

namespace FileProcessing
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter File Name");

            // iterate through file
            // parse string into 4 fields using class method
            //compute gross pay using class method
            string fileName = Console.ReadLine();
            ReadFile(fileName);
        }

        static void ReadFile(string name)
        {

            string fileName = @"C:\Academy\TestFiles\" + name;
            StreamReader inputFile = null;
            decimal totalPay = 0;
            try
            {
                inputFile = new StreamReader(fileName);
                while (!inputFile.EndOfStream)
                {
                    string fileLine = inputFile.ReadLine() ?? "";

                    Timecard newTc = Timecard.CreateTimeCard(fileLine);
                    Console.WriteLine($"Name: {newTc.Name}");
                    Console.WriteLine($"Hours Worked: {newTc.HoursWorked}");
                    Console.WriteLine($"Pay Rate: {newTc.PayRate}");

                    decimal pay = newTc.GetGrossPay();
                    Console.WriteLine($"Pay: {pay}");
                    Console.WriteLine("---------------");
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine($"File not found , this is a test message. Error: {ex.Message}");
            }
            catch (IndexOutOfRangeException ex) {
                Console.WriteLine($"Index out of range, this is a test message. Error: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error opening file, , this is a test message. Error: {ex.Message}");
            }
            finally
            {
                if (inputFile != null) inputFile.Close();
            }
        }
    }
}